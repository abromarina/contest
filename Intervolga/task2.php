<?php
	$breakProportions = FALSE;
	$crop = FALSE;
	$effect = TRUE;
	$image = imagecreatefrompng('image.png');
	$newImage = NULL;
	//Scaling with breaking proportions
	if ($breakProportions) $newImage = imagescale($image, 200, 100, IMG_BILINEAR_FIXED);
	else {
		if ($crop) {
			$newImage = imagescale($image, 200, -1, IMG_BILINEAR_FIXED);
			$newImage = imagecrop($newImage, ['x' => 0, 'y' => 50, 'width' => 200, 'height' => 100]);
		} else {
			$newImage = imagecreatetruecolor(200, 100);
			imagecolortransparent($newImage, imagecolorallocate($newImage, 0, 0, 0));
			//Creating effect with halftransparent background
			if ($effect) imagecopymerge($newImage, imagecrop(imagescale($image, 200, -1, IMG_BILINEAR_FIXED), ['x' => 0, 'y' => 50, 'width' => 200, 'height' => 100]), 0, 0, 0, 0, 200, 100, 50);
			imagecopymerge($newImage, imagescale($image, 100, -1, IMG_BILINEAR_FIXED), 50, 0, 0, 0, 100, 100, 100);
		}
	}
	ob_start();
		imagepng($newImage);
 		$image_data = ob_get_contents();
	ob_end_clean();
	$image_data_base64 = base64_encode($image_data);
	echo "<img src=\"data:image/png;base64, $image_data_base64\"/>";
?>