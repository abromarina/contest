<?php
	$link = 'https://ria.ru/20211028/avrora-1756518325.html';
	$a = 'История "Авроры" началась десять лет назад, причем не в России. В 2011-м финская компания Nokia отказалась от перспективной ОС MeeGo на базе Linux, выбрав Windows Phone. Некоторые сотрудники сочли это ошибкой, создали стартап Jolla и принялись дорабатывать ОС с открытым кодом, назвав ее Sailfish.';
	$b = mb_substr($a, 0, 180);
	//If last word inside the 180 length string is cut We should delete it
	if (!(in_array(mb_substr($a, 179, 1), array(' ', '.', ',', '!', '?', ':', ';', '-'))
		|| in_array(mb_substr($a, 180, 1), array(' ', '.', ',', '!', '?', ':', ';', '-')))) {
		$b = mb_substr($b, 0, mb_strrpos($b, ' '));
	}
	$b = trim($b) . '...';
	//Finding last word and second last word after
	$tmp = mb_substr($b, 0, mb_strrpos($b, ' '));
	$b = mb_substr($tmp, 0, mb_strrpos($tmp, ' ') + 1) . "<a href=\"$link\">" . mb_substr($tmp, mb_strrpos($tmp, ' ') + 1) . mb_substr($b, mb_strrpos($b, ' ')) . '</a>';
	echo '"' . $b . '"';
?>